package www.nisaproduction.quotify.ViewModels

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import www.nisaproduction.quotify.Models.QuoteModel
import java.nio.charset.Charset

class MainViewModel(val context: Context): ViewModel() {
    //array for quotes
    private var quotelist: Array<QuoteModel> = emptyArray()
    //array index
    private var index = 0
    init {
        quotelist = LoadListFromAssests()
    }

    private fun LoadListFromAssests(): Array<QuoteModel> {
        //This function will return an array of quotes from assests...
        //we use input stream to open file from assets.
        val inputStream = context.assets.open("quotify.json")
        //getting size of inputStream
        val size: Int = inputStream.available()
        //storing byteArray in buffer
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        //converting byteArray into String
        val json = String (buffer, Charsets.UTF_8)
        //pasrsing Json into gson
        val gson = Gson()
        return gson.fromJson(json, Array<QuoteModel>::class.java)
    }
    fun getQuote() = quotelist[index]

    fun nextQuote() = quotelist[(++index + quotelist.size) % quotelist.size]

    fun previousQuote() = quotelist[(++index + quotelist.size) % quotelist.size]
}