package www.nisaproduction.quotify.Activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import www.nisaproduction.quotify.Models.QuoteModel
import www.nisaproduction.quotify.R
import www.nisaproduction.quotify.ViewModels.MainViewModel
import www.nisaproduction.quotify.ViewModelsFactory.MainViewModelFactory

class MainActivity : AppCompatActivity() {
    lateinit var viewmodel : MainViewModel
    lateinit var getQuote : TextView
    lateinit var quoteAuthor : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //we cannot pass activity context in MainViewModelFactory(MainActivity.this) to viewModel cause
        //if screen rotate or other configuration changes happened than application lost data.
        viewmodel = ViewModelProvider(this, MainViewModelFactory(application)).get(MainViewModel::class.java)
        getQuote = findViewById(R.id.quoteText)
        quoteAuthor = findViewById(R.id.quoteAuthor)
        setQuote(viewmodel.getQuote())


    }

    fun onPrevious(view: View) {
        setQuote(viewmodel.previousQuote())
    }

    fun onNext(view: View) {
        setQuote(viewmodel.nextQuote())
    }
    fun onShare(view: View) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.setType("text/plain")
        intent.putExtra(Intent.EXTRA_TEXT,viewmodel.getQuote().text)
        startActivity(intent)

    }
    fun setQuote(quoteModel: QuoteModel){
        getQuote.text = quoteModel.text
        quoteAuthor.text = quoteModel.author
    }
}