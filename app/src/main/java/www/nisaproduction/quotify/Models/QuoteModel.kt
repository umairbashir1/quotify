package www.nisaproduction.quotify.Models

data class QuoteModel (var text: String, var author: String)
//data class have no getter and setter and also have no braces etc.
